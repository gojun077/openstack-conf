#!/bin/bash
# rdo-server-provision.sh
# Created by Jun Go gojun077@gmail.com
# Last Updated 2016-06-22


# Provision server for RDO Openstack (Kilo, Liberty or Mitaka)
# with Sheepdog distributed storage backend.

# This script should be run by the regular user from
# their HOME directory

# shellcheck source=/dev/null
source "$HOME"/openstack-conf/rdo-server-func.sh


##################
#   INSTALL
#   SOFTWARE
##################
# Install RDO repo and other packages
sudo bash "$HOME"/openstack-conf/installPackages.sh mitaka

# Clone repos from Gitlab
cloneRepos

# Build/install sheepdog from upstream github repo
bash "$HOME"/growin-server-config/build-sheepdog.sh


##################
#   OPEN PORTS
#   IN FIREWALLD
##################
sudo bash "$HOME"/openstack-conf/firewalld_openstack_rdo.sh
sudo bash "$HOME"/growin-server-config/sheepdog_firewalld.sh
# TO-DO: If no NIC's are assigned to the default zone
# in firewalld, add them to it

##################
#   SETUP
#   NETWORKING
##################
sudo systemctl stop NetworkManager
sudo systemctl disable NetworkManager
# Enable 'network' systemd service so that NIC's will come
# up at boot without NetworkManager
sudo systemctl enable network
sudo systemctl start network

##################
#   SELINUX
##################
sudo setenforce permissive
sudo sed -i "s:SELINUX=enforcing:SELINUX=permissive:g" /etc/selinux/config

##################
#   START
#   COROSYNC
##################
# make sure that you have set 'bindnetaddr' in
# /etc/corosync/corosync.conf before starting the
# Corosync daemon
sudo systemctl enable corosync
sudo systemctl start corosync

##################
#   SETUP
#   SHEEPDOG
##################
sudo bash "$HOME"/growin-server-config/setup-sheep-blockdev.sh
