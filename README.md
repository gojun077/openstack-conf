Openstack Config Files Repo
===========================
First Created: 2016-05-13
Last Updated: 2016-06-1
Maintained by: Jun Go gojun077@gmail.com

Repo for storing various Openstack conf files for different versions
of Openstack. Contains configs for Glance, Cinder, Nova, packstack, etc.
Files in the root level of this repo are related to RDO (Redhat
Distribution of Openstack) while files in subdirectories are for
other flavors of Openstack.
