# The original path for this file:
# /var/tmp/packstack/20160624-154632-277fJG/manifests/192.168.30.20_mariadb.pp

$use_subnets_value = hiera('CONFIG_USE_SUBNETS')
$use_subnets = $use_subnets_value ? {
  'y'     => true,
  default => false,
}

$service_workers = hiera('CONFIG_SERVICE_WORKERS')

Exec { timeout => hiera('DEFAULT_EXEC_TIMEOUT') }


$max_connections = $service_workers * 128


if ($::mariadb_provides_galera == 'true') {
  # Since mariadb 10.1 galera is included in main mariadb
  $mariadb_package_name = 'mariadb-server'
  #$mariadb_package_name = 'mariadb-server-galera'
  $mariadb_present      = 'present'
} else  {
  # Package mariadb-server conflicts with mariadb-galera-server
  $mariadb_package_name = 'mariadb-server'
  #$mariadb_package_name = 'mariadb-galera-server'
  $mariadb_present      = 'absent'
}

package { 'mariadb-server':
  ensure => $mariadb_present,
}

$bind_address = hiera('CONFIG_IP_VERSION') ? {
  'ipv6'  => '::0',
  default => '0.0.0.0',
  # TO-DO(mmagr): Add IPv6 support when hostnames are used
}

$mysql_root_password = hiera('CONFIG_MARIADB_PW')

class { '::mysql::server':
  package_name     => $mariadb_package_name,
  restart          => true,
  root_password    => $mysql_root_password,
  require          => Package['mariadb-server'],
  override_options => {
    'mysqld' => {
      'bind_address'           => $bind_address,
      'default_storage_engine' => 'InnoDB',
      'max_connections'        => $max_connections,
      'open_files_limit'       => '-1',
      # galera options
      'wsrep_provider'         => 'none',
      'wsrep_cluster_name'     => 'galera_cluster',
      'wsrep_sst_method'       => 'rsync',
      'wsrep_sst_auth'         => "root:${mysql_root_password}",
    },
  },
}

# deleting database users for security
# this is done in mysql::server::account_security but has problems
# when there is no fqdn, so we're defining a slightly different one here
mysql_user { [ 'root@127.0.0.1', 'root@::1', '@localhost', '@%' ]:
  ensure  => 'absent',
  require => Class['mysql::server'],
}

if ($::fqdn != '' and $::fqdn != 'localhost') {
  mysql_user { [ "root@${::fqdn}", "@${::fqdn}"]:
    ensure  => 'absent',
    require => Class['mysql::server'],
  }
}
if ($::fqdn != $::hostname and $::hostname != 'localhost') {
  mysql_user { ["root@${::hostname}", "@${::hostname}"]:
    ensure  => 'absent',
    require => Class['mysql::server'],
  }
}

class { '::keystone::db::mysql':
  user          => 'keystone_admin',
  password      => hiera('CONFIG_KEYSTONE_DB_PW'),
  allowed_hosts => '%',
  charset       => 'utf8',
}

class { '::nova::db::mysql':
  password      => hiera('CONFIG_NOVA_DB_PW'),
  host          => '%',
  allowed_hosts => '%',
  charset       => 'utf8',
}
class { '::nova::db::mysql_api':
  password      => hiera('CONFIG_NOVA_DB_PW'),
  host          => '%',
  allowed_hosts => '%',
  charset       => 'utf8',
}
class { '::cinder::db::mysql':
  password      => hiera('CONFIG_CINDER_DB_PW'),
  host          => '%',
  allowed_hosts => '%',
  charset       => 'utf8',
}

class { '::glance::db::mysql':
  password      => hiera('CONFIG_GLANCE_DB_PW'),
  host          => '%',
  allowed_hosts => '%',
  charset       => 'utf8',
}

class { '::neutron::db::mysql':
  password      => hiera('CONFIG_NEUTRON_DB_PW'),
  host          => '%',
  allowed_hosts => '%',
  dbname        => hiera('CONFIG_NEUTRON_L2_DBNAME'),
  charset       => 'utf8',
}

class { '::gnocchi::db::mysql':
  password      => hiera('CONFIG_GNOCCHI_DB_PW'),
  host          => '%',
  allowed_hosts => '%',
}

create_resources(packstack::firewall, hiera('FIREWALL_MARIADB_RULES', {}))

