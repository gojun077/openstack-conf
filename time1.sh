#!/bin/bash
# time1.sh
# Created 2016-05-13 by Jun Go and Jeon Sung-wook
# gojun077@gmail.com, codetree@gmail.com
#
# This script is intended to be wrapped by 'time' so I can
# time how long it takes to spawn an instance and boot it.

# This script creates a nova compute instance from an Ubuntu 14.04
# cloud image (RAW format) registered with Glance. The instance
# is created with an internal network interface on 10.0.0.x and
# is accessible over ssh using the 'maykey' keypair. The while
# loop checks that the new instance exists and is active before
# associating a floating ip (allowing external network access
# to the instance). The floating IP's were created before
# hand using neutron cli. Finally a while loop will run until
# it gets a response from pinging the floating ip of the new
# instance.

nova boot --image trusty-cloud-init --flavor storage-bench --key-name maykey --security-groups default --nic net-id=67f27bf2-e039-4590-b788-8ce1859ed375 test-boot-no-1
while ! nova list | grep test-boot-no-1 | grep -i ACTIVE; do :; done  
nova floating-ip-associate test-boot-no-1 192.168.30.209
while ! ping -c1 192.168.30.209 &>/dev/null; do :; done
