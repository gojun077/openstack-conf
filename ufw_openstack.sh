#!/bin/bash
# ufw_openstack.sh
# Created by Jun Go gojun077@gmail.com
# Last Updated 2016-06-21

# Script that will open ports needed by Openstack in
# UFW Firewall on Ubuntu

# This script should be run as root

#################
#   TCP PORTS
#################
AMQP=5672
CEILOM=8777
CINDER=8776
GLANCE=9292
GLANCEREG=9191
JUJU=8443
JUJUSTATE=17070
NEUTRON=9696
NOVNCPROX=6080
#NOVAEC2=8773
#NOVAMETA=8775
#NOVAISCSI=3260
#NOVAREDIS=6379
#NOVAS3=3333


#################
#   UDP PORTS
#################
CEILUDP=4952
OVSNEUTRONVXLAN=4789

TCPPORTS=($AMQP
          $CEILOM
          $CINDER
          $GLANCE
          $GLANCEREG
          $JUJU
          $JUJUSTATE
          $NEUTRON
          $NOVNCPROX
         )

UDPPORTS=($CEILUDP
          $OVSNEUTRONVXLAN
         )


#################
#   MAIN
#################
for i in ${TCPPORTS[*]}; do
  ufw allow "$i"/tcp
done

for j in ${UDPPORTS[*]}; do
  ufw allow "$j"/udp
done

# List Open Ports
ufw status
