#!/bin/bash
# firewalld_openstack_rdo.sh
# Created by Jun Go gojun077@gmail.com
# Last Updated 2016-06-23

# Script that will permanently open ports needed by
# Redhat Distribution of Openstack (RDO) in Firewalld

# Tested with Openstack Kilo RDO 7

# This script should be run as root

# DEFAULT FIREWALLD ZONE
DZONE=FedoraServer

#################
#   NETWORK
#   IFACES
#################
#EXT0=br-ex
#INT0=br-enp5s0

#################
#   TCP PORTS
#################
AMQP=5672
CEILOM=8777
CINDER=8776
GLANCE=9292
GLANCEREG=9191
NEUTRON=9696
NOVNCPROX=6080
#NOVAEC2=8773
#NOVAMETA=8775
#NOVAISCSI=3260
#NOVAREDIS=6379
#NOVAS3=3333

TCPPORTS=($AMQP
	  $CEILOM
	  $CINDER
	  $GLANCE
	  $GLANCEREG
	  $NEUTRON
	  $NOVNCPROX
	 )

# SANITY CHECK FOR NUMBER OF TCP PORTS DEFINED AND
# NO. OF ELEMENTS IN ARRAY 'TCPPORTS'
NUMTCP=7

if [ $NUMTCP -ne ${#TCPPORTS[@]} ]; then
  echo -e "Check number of elements in array TCPPORTS!\n"
  exit 1
fi

#################
#   UDP PORTS
#################
CEILUDP=4952
OVSNEUTRONVXLAN=4789

UDPPORTS=($CEILUDP
	  $OVSNEUTRONVXLAN
	 )

# SANITY CHECK FOR NUMBER OF UDP PORTS DEFINED AND
# NO. OF ELEMENTS IN ARRAY 'UDPPORTS'
NUMUDP=2

if [ $NUMUDP -ne ${#UDPPORTS[@]} ]; then
  echo -e "Check number of elements in array UDPPORTS!\n"
  exit 1
fi

# ADD NETWORK IFACES TO DEFAULT ZONE
#firewall-cmd --permanent --zone=$DZONE --add-interface="$EXT0"
#firewall-cmd --permanent --zone=$DZONE --add-interface="$INT0"

# ENABLE SERVICES REQ'D FOR OPENSTACK
# Horizon (http)
firewall-cmd --permanent --zone=$DZONE --add-service=http
# vnc-server (for some reason, enabling TCP 6080 is not enough)
firewall-cmd --permanent --zone=$DZONE --add-service=vnc-server

for i in ${TCPPORTS[*]}; do
  firewall-cmd --permanent --zone=$DZONE --add-port="$i"/tcp
done

for j in ${UDPPORTS[*]}; do
  firewall-cmd --permanent --zone=$DZONE --add-port="$j"/udp
done

# Apply permanent rules as the current runtime config
firewall-cmd --reload

# List Default Zone Firewall Info (along with ports & svcs)
firewall-cmd --list-all
