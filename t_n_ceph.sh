#!/bin/bash
# t_n_ceph.sh
# Created 2016-05-14 by Jeon Sung-wook codetree@gmail.com
# and Jun Go gojun077@gmail.com

# Launch instances with nova boot with floating IP ranging from
# 192.168.30.xxx to 192.168.30.yyy where 'xxx' is parameter '$1'
# and 'yyy' is parameter '$2'

# USAGE ./t_n_ceph.sh 205 210

# The above example creates instances named
# 'test-boot-no-205' ~ 'test-boot-no-210'. In the script
# 'time_tmp_ceph.sh', the internal network IP's for the instances
# will be automatically assigned by 'nova boot' while the
# floating IP's will be associated to instances by name using
# 'nova floating-ip-associate'

NUM_VM=$(($2 - $1 + 1))

for ((i = $1; i <= $2; i++)); do
  (time ./time_tmp_ceph.sh test-boot-no-$i 192.168.30.$i) &> test${NUM_VM}_no${i} &
done

echo ====================================================================
for ((i = $1; i <= $2; i++)); do
   echo ping -c1 192.168.30.$i
   while ! ping -c1 192.168.30.$i &>/dev/null; do
     : # no-op
   done
   echo ping -c1 192.168.30.$i
done

