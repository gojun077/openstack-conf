#!/bin/bash
# rdo-server-func.sh
# Created by Jun Go gojun077@gmail.com
# Last Updated 2016-06-22

# This script contains functions that should be sourced from
# another script that will actually provision a server for RDO
# After an installation from a Fedora 22+ server DVD image
# functions in this file can be called to install necessary
# packages, # set up the firewall, set up the network, and
# make other necessary settings for a specified version of
# Redhat Distribution of Openstack (RDO).

cloneRepos()
{
  OSPCONF="https://gitlab.com/gojun077/openstack-conf.git"
  GROWINCONF="https://gitlab.com/gojun077/growin-server-config.git"
  CLONEPATH="/home/fedjun/"

  if [ -d /home/fedjun/openstack-conf ]; then
    echo -e "openstack-conf dir already exists\n"
  else
    git clone $OSPCONF $CLONEPATH/openstack-conf
  fi

  if [ -d /home/fedjun/growin-server-config ]; then
    echo -e "growin-server-config dir already exists\n"
  else
    git clone $GROWINCONF $CLONEPATH/growin-server-config
  fi

}

