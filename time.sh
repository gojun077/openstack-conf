#!/bin/bash
nova boot --image trusty-cloud-init --flavor storage-bench --key-name maykey --security-groups default --nic net-id=67f27bf2-e039-4590-b788-8ce1859ed375,v4-fixed-ip=10.0.0.51 test-boot-no12
while ! nova list | grep test-boot-no12 | grep -i ACTIVE; do :; done  
nova floating-ip-associate test-boot-no12 192.168.30.225
while ! ping -c1 192.168.30.225 &>/dev/null; do :; done
