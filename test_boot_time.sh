#!/bin/bash

# test_boot_time.sh
# Creation date: 2016-05-14
# Created by: Jeon Sung-wook codetree@gmail.com
# Maintained by: Jun Go gojun077@gmail.com
# Last Changed: 2016-05-14

# This wrapper script calls t_n_ceph.sh to create a number
# of nova instances with floating IP's between
# parameter 1 and 2 (inclusive). After t_n_ceph.sh
# completes, cleanup script t_del_n.sh is invoked
# to delete the nova instances just created.
# This script also prints the time required to
# complete t_n_ceph.sh for each different number of
# nodes (1, 10, 20, 30, 40).

echo node 1
time ./t_n_ceph.sh  210 210
./t_del_n.sh 210 210
sleep 3

echo node 10
time ./t_n_ceph.sh  210 219
./t_del_n.sh 210 219
sleep 3

echo node 20
time ./t_n_ceph.sh  210 229
./t_del_n.sh 210 229
sleep 3

echo node 30
time ./t_n_ceph.sh  210 239
./t_del_n.sh 210 239
sleep 3

echo node 40
time ./t_n_ceph.sh  210 249
./t_del_n.sh 210 249
