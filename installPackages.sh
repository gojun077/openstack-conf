#!/bin/bash
#installPackages.sh

# Created by Jun Go gojun077@gmail.com
# Last Updated 2016-06-22

# USAGE: install-packages <rdo version - kilo, liberty, or mitaka>

kilourl="https://repos.fedorapeople.org/repos/openstack/openstack-kilo/"
kilorepo="rdo-release-kilo-2.noarch.rpm"
lbtyurl="https://repos.fedorapeople.org/repos/openstack/openstack-liberty/"
lbtyrepo="rdo-release-liberty-3.noarch.rpm"
mitakaurl="https://repos.fedorapeople.org/repos/openstack/openstack-mitaka/"
mitakarepo="rdo-release-mitaka-3.noarch.rpm"

if [ "$1" = "kilo" ]; then
  dnf install -y $kilourl$kilorepo
elif [ "$1" = "liberty" ]; then
  dnf install -y $lbtyurl$lbtyrepo
elif [ "$1" = "mitaka" ]; then
  dnf install -y $mitakaurl$mitakarepo
else
  echo -e "You must specify RDO version (kilo, liberty, mitaka)"
  exit 1
fi

PKGLIST=(git
         ntp
         openstack-packstack
         sheepdog
         vim
        )

for i in ${PKGLIST[*]}; do
  dnf install -y "$i"
done
