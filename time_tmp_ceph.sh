#!/bin/bash
# time_tmp_ceph.sh
# Created 2016-05-13 by Jun Go and Jeon Sung-wook
# gojun077@gmail.com, codetree@gmail.com
#
# This script is intended to be wrapped by 'time' so I can
# time how long it takes to spawn an instance and boot it.

# This script creates a nova compute instance from an Ubuntu 14.04
# cloud image (RAW format) registered with Glance. The instance
# is created with an internal network interface on 10.0.0.x and
# is accessible over ssh using the 'maykey' keypair. The 1st while
# loop checks that the new instance exists and is active before
# associating a floating ip (allowing external network access
# to the instance) to it. The 2nd while loop will run until it
# gets ping response from the floating ip of the new instance.

# This version of the script uses a Glance image that is stored
# on a ceph storage backend.

# USAGE: ./time_tmp_ceph.sh nameOfInstance floatingIP

nova boot --image trusty-cloud-init-ceph --flavor storage-bench --key-name maykey --security-groups default --nic net-id=67f27bf2-e039-4590-b788-8ce1859ed375 $1
while ! nova list | grep $1 | grep -i ACTIVE; do :; done
nova floating-ip-associate $1 $2
while ! ping -c1 $2 &>/dev/null; do :; done
