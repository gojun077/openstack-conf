# cfme_add_provider.py
# Python 3 script that adds an Openstack cloud provider through
# the CFME API (v2.1.0+) for CFME v4

import requests
import pprint
import json

payload = json.dumps({'action' : 'create',
                     'resource' :
                      {"name" : "openstack-cfme",
                       "type" : "ManageIQ::Providers::Openstack::CloudManager",
                       "hostname" : "192.168.40.198",
                       "ipaddress" : "192.168.40.198",
                       "port" : "5000",
                       "credentials" :
                       [ { "userid" : "admin",
                           "password" : "admin" },
                         { "userid" : "guest",
                           "password" : "guest",
                           "auth_type" : "amqp" }]
                       }
                      }
                     )

resp = requests.post('https://192.168.40.250/api/providers',
                     auth=('admin', 'smartvm'),
                     data=payload,
                     verify=False)

pprint.pprint(resp.json())
