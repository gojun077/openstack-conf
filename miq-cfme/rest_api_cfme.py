# rest_api_cfme.py
# Python3 script to test communicating with Cloudforms
# web mgmt iface over API

import requests
import pprint
import json


# Show all available services
# 'verify=False' - make API call without verifying SSL cert; the CFME
# install uses a private cert
# resp = requests.get('https://192.168.40.250/api/services',\
#    auth=('admin', 'smartvm'), verify=False)

# pretty print json w/ pprint
# pprint.pprint(resp.json())

# Create a category

# payload1 = json.dumps({'action':'create',
#            'resource':{'name':'py0', 'description':'category py0'}})
# resp1 = requests.post('https://192.168.40.250/api/categories',
#                       auth=('admin', 'smartvm'),
#                       data = payload1,
#                       verify=False)

# pprint.pprint(resp1.json())

# Delete a category

payload2 = json.dumps({'action' : 'delete'})
resp2 = requests.post('https://192.168.40.250/api/categories/126',
                      auth=('admin', 'smartvm'),
                      data=payload2,
                      verify=False)

pprint.pprint(resp2.json())
