# cfme_delete_provider.py
# Python 3 script that deletes an Openstack cloud provider through
# the CFME API (v2.1.0+) for CFME v4

import requests
import pprint
import json

payload = json.dumps({'action' : 'delete'})

# The following assumes that the Cloud Provider you want to delete
# has a provider id of '6'
resp = requests.post('https://192.168.40.250/api/providers/6',
                     auth=('admin', 'smartvm'),
                     data=payload,
                     verify=False)

pprint.pprint(resp.json())
