* Openstack + CFME API/cmd 짬봉

** Image 관련 명령어
   - 오픈스텍
     아래 명령어들은 오픈스텍 서버에서 구동해야 함
     (192.168.40.198 fedjun:fedjun)
     + 이미지 등록 (cirrOS 이미지)
       $HOME 경로에서 명령어 실행:

       #+BEGIN_SRC shell
       source keystonerc_admin
       wget -P $HOME/temp/ http://download.cirros-cloud.net/0.3.4/cirros-0.3.4-x86_64-disk.img
       glance --debug image-create --name "cirros-0.3.4-x86_64" --file $HOME/temp/cirros-0.3.4-x86_64-disk.img --disk-format qcow2 --container-format bare --progress
       #+END_SRC shell

     + 모든 이미지 확인
       #+BEGIN_SRC shell
       glance --debug image-list
       #+END_SRC shell

     + 특정 이미지 확인 (상세)
       #+BEGIN_SRC shell
       glance --debug image-show <IMAGE_ID)
       #+END_SRC shell

     + 이미지 수정
       #+BEGIN_SRC shell
       glance --debug image-update <IMAGE_ID> --name <NAME> --container-format <CONTAINER_FORMAT>
       #+END_SRC shell

     + 이미지 삭제
       #+BEGIN_SRC shell
       glance --os-image-api-version 2 image-delete <IMAGE_ID>
       #+END_SRC shell

** Openstack Instance(VM) 관련 명령어
   - 오픈스텍
     아래 명령어들은 오픈스텍 서버에서 구동해야 함
     (192.168.40.198 fedjun:fedjun)

     + Flavor 조회하기
       디폴트 flavor 종류: m1.tiny, m1.small, m1.medium, m1.large, m1.xlarge
       $HOME 경로에서 명령어 실행:
       #+BEGIN_SRC shell
       source keystonerc_admin
       nova --debug flavor-list
       #+END_SRC shell

     + Image 조회하기
       #+BEGIN_SRC shell
       source keystonerc_admin
       nova --debug image-list
       #+END_SRC shell

     + 모든 Instance/VM 조회하기
       #+BEGIN_SRC shell
       source keystonerc_admin
       nova --debug list
       #+END_SRC shell

     + Network 정보 조회하기
       Network_id, Network_name, Network_subnet
       #+BEGIN_SRC shell
       source keystonerc_admin
       neutron --debug net-list
       #+END_SRC shell

     + Security group 조회하기
       #+BEGIN_SRC shell
       source keystonerc_admin
       nova --debug secgroup-list
       #+END_SRC shell

     + Instance(VM) 생성하기
       윗 정보 (Flavor, Image, Network, Security Group)를 명시하여 instance
       만들 수 있습니다.
       #+BEGIN_SRC shell
       nova --debug boot --flavor m1.tiny --image test_image1 --nic net-id=00007257-eefb-4f8f-9b97-c006710ad657 --security-group default cli-instance1
       #+END_SRC shell
       +--------------------------------------+----------------------------------------------------+
       | Property                             | Value                                              |
       +--------------------------------------+----------------------------------------------------+
       | OS-DCF:diskConfig                    | MANUAL                                             |
       | OS-EXT-AZ:availability_zone          | nova                                               |
       | OS-EXT-SRV-ATTR:host                 | -                                                  |
       | OS-EXT-SRV-ATTR:hypervisor_hostname  | -                                                  |
       | OS-EXT-SRV-ATTR:instance_name        | instance-0000000a                                  |
       | OS-EXT-STS:power_state               | 0                                                  |
       | OS-EXT-STS:task_state                | scheduling                                         |
       | OS-EXT-STS:vm_state                  | building                                           |
       | OS-SRV-USG:launched_at               | -                                                  |
       | OS-SRV-USG:terminated_at             | -                                                  |
       | accessIPv4                           |                                                    |
       | accessIPv6                           |                                                    |
       | adminPass                            | 9k5nNR9D8TyM                                       |
       | config_drive                         |                                                    |
       | created                              | 2016-01-12T06:56:12Z                               |
       | flavor                               | m1.tiny (1)                                        |
       | hostId                               |                                                    |
       | id                                   | d5bc4635-6904-44be-bf41-004a22ad4ef1               |
       | image                                | test_image1 (12d43bbc-9600-4c49-882a-2addccea8b79) |
       | key_name                             | -                                                  |
       | metadata                             | {}                                                 |
       | name                                 | cli-instance1                                      |
       | os-extended-volumes:volumes_attached | []                                                 |
       | progress                             | 0                                                  |
       | security_groups                      | default                                            |
       | status                               | BUILD                                              |
       | tenant_id                            | 1e6f3fe16a7b4cb7ad2feb797172a7a0                   |
       | updated                              | 2016-01-12T06:56:13Z                               |
       | user_id                              | 9811950e0fd64d1ea7bf246ed7b7e6c0                   |
       +--------------------------------------+----------------------------------------------------+

     + Instance(VM) 수정하기 (이름만)
       http://docs.openstack.org/cli-reference/nova.html
       #+BEGIN_SRC shell
       nova --debug rename <server_ID> <new_name>
       #+END_SRC shell

** Cloud Provider 관련 명령어
   - CFME (192.168.40.250)
     + 모든 Cloud Provider 확인
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X GET -H "Accept: application/json" \
         https://192.168.40.250/api/providers
       #+END_SRC shell

     + 특정 Cloud Provider 확인
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X GET -H "Accept: application/json" \
         https://192.168.40.250/api/providers/<Provider ID>
       #+END_SRC shell

     + Cloud Provider 추가
       Note: "hostname" field가 비어 있으면 안된다. Hostname을 입력한다면
       FQDN 형식(DNS 검색이 가능한 이름)에 부합해야 한다. FQDN 이름 없으면
       그냥 IP 주소를 입력하세요.
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
        -i -k -X POST -H "Accept: application/json" \
        -d '{ "action" : "create", "resource" : { "type" : "ManageIQ::Providers::Openstack::CloudManager", "name" : "openstack-cfme", "hostname" : "192.168.40.198", "ipaddress" : "192.168.40.198", "port" : "5000", "credentials" : [ { "userid" : "admin", "password" : "admin" }, { "userid" : "guest", "password" : "guest", "auth_type" : "amqp" } ]  } }' \
        https://192.168.40.250/api/providers
       #+END_SRC shell

     + Cloud Provider Refresh
       (상태 확인: 문제 없으면 success, 문제 있으면 fail)
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X POST -H "Accept: application/json" \
         -d '{ "action" : "refresh" }' \
         https://192.168.40.250/api/providers/<provider_ID>
       #+END_SRC shell

     + Cloud Provider 삭제
       + POST 방법
         #+BEGIN_SRC shell
         curl --user admin:smartvm \
           -i -k -X POST -H "Accept: application/json" \
           -d '{ "action" : "delete" }' \
           https://192.168.40.250/api/providers/<Provider ID>
       #+END_SRC shell

       + DELETE 방법
         #+BEGIN_SRC shell
         curl --user admin:smartvm \
           -i -k -X DELETE -H "Accept: application/json" \
           https://192.168.40.250/api/providers/<Provider ID>
         #+END_SRC shell

     + Cloud Provider Edit
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X POST -H "Accept: application/json" \
         -d '{ "action" : "edit", "resource" : { "name" : "changed name 1", "hostname" : "192.168.40.77", "ipaddress" : "192.168.40.77", "port" : "5000", "credentials" : [ { "userid" : "yo", "password" : "yo" }, { "userid" : "guest", "password" : "guest", "auth_type" : "amqp" } ]  } }' \
         https://192.168.40.250/api/providers/<provider_ID>
       #+END_SRC shell

** CFME Category 관련 명령어
   https://github.com/ManageIQ/manageiq_docs/blob/master/api/reference/categories_tags.adoc#deleting-categories-tags
   - CFME web server 통해 (192.168.40.250)
     + 모든 Category ID 확인
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X GET -H "Accept: application/json" \
         https://192.168.40.250/api/categories
       #+END_SRC shell

     + 특정한 Category ID에 대한 상세 정보
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X GET -H "Accept: application/json" \
         https://192.168.40.250/api/categories/<cat_ID>
       #+END_SRC shell

     + Category 만들기
       Category 이름 정할 때 소문자와 언더바 '_'만 허용됨.
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X POST -H "Accept: application/json" \
         -d '{ "action" : "create", "resource" : { "name" : "gr0", "description" : "Category gr0" } }' \
         https://192.168.40.250/api/categories
       #+END_SRC shell

     + Category 수정하기
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X POST -H "Accept: application/json" \
         -d '{ "action" : "edit", "resource" : { "description" : "edited cat description" } }' \
         https://192.168.40.250/api/categories/<cat_ID>
       #+END_SRC shell

     + Category 삭제하기 (방법 1, DELETE)
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X DELETE -H "Accept: application/json" \
         https://192.168.40.250/api/categories/<Category ID; Category 이름과 다름!>
       #+END_SRC shell

     + Category 삭제하기 (방법 2, POST)
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X POST -H "Accept: application/json" \
         -d '{ "action" : "delete" }' \
         https://192.168.40.250/api/categories/<Category ID; Category 이름과 다름!>
       #+END_SRC shell

     + 모든 Tag ID 확인
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X GET -H "Accept: application/json" \
         https://192.168.40.250/api/tags
       #+END_SRC shell

     + 특정한 Tag ID에 대한 상세 정보
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X GET -H "Accept: application/json" \
         https://192.168.40.250/api/tags/<tag_ID>
       #+END_SRC shell

     + Tag 만들기 (특정한 Category 밑으로 생성됨)
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X POST -H "Accept: application/json" \
         -d '{ "action" : "create", "resource" : { "name" : "tag_gr0", "description" : "tag for category gr0" } }' \
         https://192.168.40.250/api/categories/<cat_ID>/tags
       #+END_SRC shell

     + Tag 수정하기
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X POST -H "Accept: application/json" \
         -d '{ "action" : "edit", "resource" : { "name" : "updated_test_tag" } }' \
         https://192.168.40.250/api/tags/<tag_ID>
       #+END_SRC shell

     + Tag 삭제하기
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X POST -H "Accept: application/json" \
         -d '{ "action" : "delete" }' \
         https://192.168.40.250/api/tags/<tag_ID>
       #+END_SRC shell

** CFME Availability Zone (Openstack zone)관련 명령어
   참고: CFME 'availability_zone'은 Openstack 'zone'과 같고,
   CFME 'zone'은 Openstack 'zone'과 별개입니다!
   - CFME web server 통해 (192.168.40.250)
     + 모든 Availability Zone ID 확인
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X GET -H "Accept: application/json" \
         https://192.168.40.250/api/availability_zones
       #+END_SRC shell

     + 특정한 Availability Zone ID에 대한 상세 정보
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X GET -H "Accept: application/json" \
         https://192.168.40.250/api/availability_zones/<A_z ID>
       #+END_SRC shell

** CFME zone 관련 명령어
   참고: CFME 'zone'은 Openstack 'zone'과 다르다!
   - CFME web server 통해 (192.168.40.250)
     + 모든 CFME zone ID 확인
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X GET -H "Accept: application/json" \
         https://192.168.40.250/api/zones
       #+END_SRC shell

     + 특정한 CFME zone ID에 대한 상세 정보
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X GET -H "Accept: application/json" \
         https://192.168.40.250/api/zones/<zone_ID>
       #+END_SRC shell

** CFME Instance (Provision Request) 관련 명령어
   *참고 사항* CFME 내에서 Instance(VM) 생성할려면 Cloud Provider ->
   'Provision Request'를 선택해야 함. 또는 Instance(VM) 삭제할려면
   Cloud Provider -> 'Retire Selected Items' 선택해야 함
   API v1에 'retire' method 있었지만 v2 API에 2016.01까지는
   'create' method만 지원됨

   + Provision_Request 죄회하기
     #+BEGIN_SRC shell
     curl --user admin:smartvm \
       -i -k -X GET -H "Accept: application/json" \
       https://192.168.10.62/api/provision_requests
     #+END_SRC shell


   + Provision_Request로 Instance(VM) 생성하기
     Openstack에서 Instance 삭제
     #+BEGIN_SRC shell
     curl --user admin:smartvm \
       -i -k -X POST -H "Accept: application/json" \
       -d '{ "action" : "create", "resources" : [{.....
       https://192.168.10.62/api/provision_requests
     #+END_SRC shell

** CFME VM 관련 명령어
   *참고 사항* CFME 내에서 start, stop, suspend, scan, delete 등을 수행할
   수 있지만 create 기능은 없습니다. VM 만들려면 아랫단인 openstack, RHEV나
   AWS에서 만들어야 합니다.
   - CFME web server 통해 (192.168.40.250)
     + 모든 VM 확인
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X GET -H "Accept: application/json" \
         https://192.168.10.62/api/vms
       #+END_SRC shel

     + 특정한 VM ID에 대한 상세 정보
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X GET -H "Accept: application/json" \
         https://192.168.40.250/api/vms/<vm_ID>
       #+END_SRC shell

     + 특정한 VM ID scan 하기
       'scan'하면 task_id와 task_href 정보 확인할 수 있음
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X POST -H "Accept: application/json" \
         -d '{ "action" : "scan" }' \
         https://192.168.40.250/api/vms/<vm_ID>
       #+END_SRC shell

     + 특정한 VM 시작하기
       시작이 되는데 이 사실은 Horizon에서만 감지되고 CFME web UI에서
       "정지" 상태로 남아 있음.
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X POST -H "Accept: application/json" \
         -d '{ "action" : "start" }' \
         https://192.168.40.250/api/vms/<vm_ID>
       #+END_SRC shell

     + 특정한 VM 정지하기 (문제가 있음)
       cirros_four (vm id 4) 정지 시킬 수 없지만 cirrostiny (vm id 5)
       정지 시킬 수 있습니다.
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X POST -H "Accept: application/json" \
         -d '{ "action" : "stop" }' \
         https://192.168.40.250/api/vms/<vm_ID>
       #+END_SRC shell

     + 특정한 VM 일시중지 (문제 있음)
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X POST -H "Accept: application/json" \
         -d '{ "action" : "suspend" }' \
         https://192.168.40.250/api/vms/<vm_ID>
       #+END_SRC shell

     + 특정한 VM 삭제하기
       주의 사항: VM/Instance를 CFME 내에 '삭제'하면, CFME web UI에서
       나타나지 않지만 Openstack Horizon UI에서 여전히 존재합니다.
       #+BEGIN_SRC shell
       curl --user admin:smartvm \
         -i -k -X POST -H "Accept: application/json" \
         -d '{ "action" : "delete" }' \
         https://192.168.40.250/api/vms/<vm_ID>
       #+END_SRC shell
