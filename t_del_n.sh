#!/bin/bash

# t_n_del.sh
# Creation date: 2016-05-14
# Created by: Jeon Sung-wook codetree@gmail.com
# Maintained by: Jun Go gojun077@gmail.com

# This script will delete nova instances which are
# named 'test-boot-no-x' where 'x' is some integer
# t_n_del.sh takes two parameters, start and
# ending value (inclusive) for a range of ints.

# Usage: ./t_n_del.sh 1 2
# This will invoke 'nova delete test-boot-no-{1,2}'

for ((i = $1; i <= $2; i++)); do
  nova delete test-boot-no-$i 
done

